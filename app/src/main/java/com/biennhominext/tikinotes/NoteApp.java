package com.biennhominext.tikinotes;

import android.app.Application;

import com.biennhominext.tikinotes.data.AppDatabase;

/**
 * Created by bien on 6/26/2017.
 */

public class NoteApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.initializeContext(getApplicationContext());
    }
}
