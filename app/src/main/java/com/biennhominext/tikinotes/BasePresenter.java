package com.biennhominext.tikinotes;

/**
 * Created by bien on 6/26/2017.
 */

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
