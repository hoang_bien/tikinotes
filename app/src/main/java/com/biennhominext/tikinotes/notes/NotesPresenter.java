package com.biennhominext.tikinotes.notes;

import android.util.Log;

import com.biennhominext.tikinotes.addeditnotes.AddEditNoteContract;
import com.biennhominext.tikinotes.data.Note;
import com.biennhominext.tikinotes.data.NoteDAO;
import com.biennhominext.tikinotes.util.SchedulerProvider;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 6/26/2017.
 */

public class NotesPresenter implements NotesContract.Presenter {
    private SchedulerProvider mProvider;
    private NoteDAO mDb;
    private NotesContract.View mView;
    private CompositeDisposable mDisposables;

    public NotesPresenter(NoteDAO dao, SchedulerProvider schedulerProvider
            , NotesContract.View view) {
        mDb = dao;
        mProvider = schedulerProvider;
        mView = view;
        mDisposables = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        loadNotes();
    }

    @Override
    public void unsubscribe() {
        mDisposables.clear();
    }

    @Override
    public void loadNotes() {
        mDisposables.clear();
        mDisposables.add(mDb.getAllNotes()
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .subscribe(mView::showNotes));
    }

    @Override
    public Single<Integer> deleteNotes(List<Note> notes) {
        return Single.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int res = mDb.deleteNote(notes);
                return res;
            }
        }).subscribeOn(mProvider.io()).observeOn(mProvider.ui());
    }
}
