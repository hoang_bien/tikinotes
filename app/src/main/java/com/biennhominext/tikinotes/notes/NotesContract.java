package com.biennhominext.tikinotes.notes;

import com.biennhominext.tikinotes.BasePresenter;
import com.biennhominext.tikinotes.BaseView;
import com.biennhominext.tikinotes.data.Note;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by bien on 6/26/2017.
 */

public interface NotesContract {
    interface View extends BaseView<Presenter>{
        void showNotes(List<Note> notes);
    }

    interface Presenter extends BasePresenter{
        void loadNotes();

        Single<Integer> deleteNotes(List<Note> notes);
    }

}
