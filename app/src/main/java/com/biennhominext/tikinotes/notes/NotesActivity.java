package com.biennhominext.tikinotes.notes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.biennhominext.tikinotes.R;
import com.biennhominext.tikinotes.addeditnotes.AddEditNoteActivity;
import com.biennhominext.tikinotes.data.AppDatabase;
import com.biennhominext.tikinotes.data.Note;
import com.biennhominext.tikinotes.util.SchedulerProvider;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotesActivity extends AppCompatActivity implements NotesContract.View, NotesAdapter.OnItemClickListener {
    @BindView(R.id.recycler_notes)
    RecyclerView mRecycler;

    private NotesAdapter mAdapter;
    private NotesContract.Presenter mPresenter;
    private ActionMode mActionMode;
    private FloatingActionButton mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        mFab = findViewById(R.id.fab);
        mFab.setOnClickListener(view -> {
            startActivity(new Intent(NotesActivity.this, AddEditNoteActivity.class));
        });
        mPresenter = new NotesPresenter(AppDatabase.get().noteDAO(),SchedulerProvider.getInstance(),this);
        mAdapter = new NotesAdapter();
        mRecycler.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        mRecycler.setAdapter(mAdapter);
        mAdapter.setListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemStateChanged(Note note) {
        mActionMode.setTitle(getString(R.string.format_note_count_selected,mAdapter.getSelectedCount()));
        if (mAdapter.getSelectedCount() == 0){
            mAdapter.setSelectable(false);
            if (mActionMode != null){
                mActionMode.finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(NotesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showNotes(List<Note> notes) {
        mAdapter.setData(notes);
    }

    @Override
    public void onItemClick(Note note) {
        Intent intent = new Intent(this,AddEditNoteActivity.class);
        intent.putExtra(AddEditNoteActivity.EXTRA_NOTE_ID,note.getUid());
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(Note note) {
        mActionMode = startSupportActionMode(mCallback);
    }

    private ActionMode.Callback mCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.menu_action_mode,menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mAdapter.setSelectable(true);
            mFab.hide();
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (item.getItemId() == R.id.action_delete){
                List<Note> notes = mAdapter.getSelectedNotes();
                new AlertDialog.Builder(NotesActivity.this)
                        .setTitle("Confirm")
                        .setMessage(getString(R.string.msg_items_deleted,notes.size()))
                        .setPositiveButton("OK",(dialogInterface,i)->{
                            mPresenter.deleteNotes(notes).subscribe(deleted->{
                                Toast.makeText(NotesActivity.this,"Deleted " + deleted + " items",Toast.LENGTH_SHORT).show();
                            });
                            mActionMode.finish();
                        })
                        .setNegativeButton("Cancel",null)
                        .show();
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.setSelectable(false);
            mFab.show();
        }
    };
}
