package com.biennhominext.tikinotes.notes;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biennhominext.tikinotes.R;
import com.biennhominext.tikinotes.data.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bien on 6/26/2017.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    private List<Note> mNotes;
    private OnItemClickListener mListener;
    private SparseBooleanArray mSelecteds;
    private boolean mSelectable = false;


    public NotesAdapter() {
        mSelecteds = new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.itemView.setOnClickListener(__ -> {
            if (mListener != null) {
                final int position = holder.getAdapterPosition();
                if (mSelectable) {
                    final boolean state = mSelecteds.get(position);
                    mSelecteds.put(position, !state);
                    mListener.onItemStateChanged(mNotes.get(position));
                    notifyItemChanged(position);
                } else {
                    mListener.onItemClick(mNotes.get(position));
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mListener != null && !mSelectable) {
                    final int position = holder.getAdapterPosition();
                    mListener.onItemLongClick(mNotes.get(position));
                    mSelecteds.put(position, true);
                    mListener.onItemStateChanged(mNotes.get(position));
                }
                return true;
            }
        });
        return holder;
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public void setData(List<Note> notes) {
        mNotes = notes;
        mSelecteds.clear();
        notifyDataSetChanged();
    }

    public void setSelectable(boolean selectable) {
        this.mSelectable = selectable;
        if (!mSelectable){
            mSelecteds.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Note item = mNotes.get(position);

        holder.mTextTitle.setText(item.getTitle());
        holder.mTextDesc.setText(item.getDescription());

        if (mSelectable && mSelecteds.get(position)) {
            holder.mContainer.setBackgroundResource(R.color.bg_card_selected);
        } else {
            holder.mContainer.setBackgroundColor(0xffffffff);
        }
    }

    @Override
    public int getItemCount() {
        return mNotes == null ? 0 : mNotes.size();
    }

    public int getSelectedCount() {
        int count = 0;
        for (int i = 0; i < mNotes.size(); i++) {
            if (mSelecteds.get(i)){
                count++;
            }
        }
        return count;
    }

    public List<Note> getSelectedNotes() {
        List<Note> selectedNotes = new ArrayList<>();
        for (int i = 0; i < mNotes.size(); i++) {
            if (mSelecteds.get(i)){
                selectedNotes.add(mNotes.get(i));
            }
        }
        return selectedNotes;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextTitle;
        TextView mTextDesc;
        View mContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextTitle = itemView.findViewById(R.id.text_title);
            mTextDesc = itemView.findViewById(R.id.text_description);
            mContainer = itemView.findViewById(R.id.container);
        }
    }

    public SparseBooleanArray getSelecteds() {
        return mSelecteds;
    }

    public interface OnItemClickListener {
        void onItemClick(Note note);

        void onItemLongClick(Note note);

        void onItemStateChanged(Note note);
    }
}
