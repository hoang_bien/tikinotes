package com.biennhominext.tikinotes;

/**
 * Created by bien on 6/26/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
