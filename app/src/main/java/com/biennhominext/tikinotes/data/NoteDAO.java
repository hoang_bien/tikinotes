package com.biennhominext.tikinotes.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by bien on 6/26/2017.
 */
@Dao
public interface NoteDAO {
    @Query("SELECT * from note ORDER BY time DESC")
    Flowable<List<Note>> getAllNotes();

    @Query("SELECT * from note WHERE uid=:uid LIMIT 1")
    Flowable<Note> getNote(String uid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addNote(Note note);

    @Update
    int updateNote(Note note);

    @Delete
    void deleteNote(Note note);

    @Delete
    int deleteNote(List<Note> notes);



}
