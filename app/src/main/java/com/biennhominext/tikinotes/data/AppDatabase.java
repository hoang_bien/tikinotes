package com.biennhominext.tikinotes.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

/**
 * Created by bien on 6/26/2017.
 */

@Database(entities = {Note.class}, version = 1)
@TypeConverters({TimeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase sInstance;
    private static Context sContext;


    public static void initializeContext(Context context) {
        sContext = context;
    }

    public static AppDatabase get() {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(sContext, AppDatabase.class, "notes-db")
                    .build();
        }
        return sInstance;
    }

    public abstract NoteDAO noteDAO();
}
