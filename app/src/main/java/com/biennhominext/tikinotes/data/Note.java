package com.biennhominext.tikinotes.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

/**
 * Created by bien on 6/26/2017.
 */
@Entity
public class Note {
    @PrimaryKey
    private String uid;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "desc")
    private String description;

    @ColumnInfo(name = "time")
    private Date time;

    public Note(String uid, String title, String description, Date time) {
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.time = time;
    }

    @Ignore
    public Note(String uid) {
        this.uid = uid;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
