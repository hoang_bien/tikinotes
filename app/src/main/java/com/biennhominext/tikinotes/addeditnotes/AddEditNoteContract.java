package com.biennhominext.tikinotes.addeditnotes;

import com.biennhominext.tikinotes.BasePresenter;
import com.biennhominext.tikinotes.BaseView;

import io.reactivex.Single;

/**
 * Created by bien on 6/26/2017.
 */

public interface AddEditNoteContract {

    interface View extends BaseView<Presenter> {
        void setTitle(String title);

        void setDescription(String desc);

        void showEmptyNoteError();
    }

    interface Presenter extends BasePresenter {
        Single<Boolean> saveNote(String title, String description);

        void populateNote();

        Single<Boolean> deleteNote();
    }
}
