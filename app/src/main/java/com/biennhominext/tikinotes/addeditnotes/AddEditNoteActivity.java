package com.biennhominext.tikinotes.addeditnotes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biennhominext.tikinotes.R;
import com.biennhominext.tikinotes.data.AppDatabase;
import com.biennhominext.tikinotes.util.SchedulerProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bien on 6/26/2017.
 */

public class AddEditNoteActivity extends AppCompatActivity implements AddEditNoteContract.View {
    public static final String EXTRA_NOTE_ID = "note_id";
    @BindView(R.id.edit_title)
    EditText mEditTitle;
    @BindView(R.id.edit_desc)
    EditText mEditDesc;

    private String mNoteId;
    private AddEditNoteContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        ButterKnife.bind(this);
        mNoteId = getIntent().getStringExtra(EXTRA_NOTE_ID);
        mPresenter = new AddEditNotePresenter(mNoteId, AppDatabase.get().noteDAO()
                , SchedulerProvider.getInstance(), this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_edit_note, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                mPresenter.saveNote(mEditTitle.getText().toString()
                        , mEditDesc.getText().toString())
                        .subscribe(sucess -> {
                            if (sucess) {
                                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });


                break;
            case R.id.action_delete:
                mPresenter.deleteNote().subscribe(deleted->{
                    Log.d("AddEditNoteActivity","Deleted: " + deleted);
                });
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(String title) {
        mEditTitle.setText(title);
    }

    @Override
    public void setDescription(String desc) {
        mEditDesc.setText(desc);
    }

    @Override
    public void showEmptyNoteError() {
        Toast.makeText(this, "Couldn't load this item", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(AddEditNoteContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
