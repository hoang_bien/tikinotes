package com.biennhominext.tikinotes.addeditnotes;

import com.biennhominext.tikinotes.data.Note;
import com.biennhominext.tikinotes.data.NoteDAO;
import com.biennhominext.tikinotes.util.SchedulerProvider;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 6/26/2017.
 */

public class AddEditNotePresenter implements AddEditNoteContract.Presenter {

    private String mNoteId;
    private NoteDAO mDb;
    private SchedulerProvider mProvider;
    private AddEditNoteContract.View mView;
    private CompositeDisposable mSubscriptions;

    public AddEditNotePresenter(String mNoteId, NoteDAO dao, SchedulerProvider schedulerProvider
            , AddEditNoteContract.View view) {
        this.mNoteId = mNoteId;
        mDb = dao;
        mProvider = schedulerProvider;
        mView = view;
        mSubscriptions = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        if (!isNewNote()) {
            populateNote();
        }

    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }


    @Override
    public Single<Boolean> saveNote(String title, String description) {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                long inserted = -1;
                if (isNewNote()) {
                    mNoteId = UUID.randomUUID().toString();
                    Note note = new Note(mNoteId, title, description, new Date());
                    inserted = mDb.addNote(note);
                } else {
                    Note note = new Note(mNoteId, title, description, new Date());
                    inserted = mDb.updateNote(note);
                }
                return inserted != -1;
            }
        }).subscribeOn(mProvider.io()).observeOn(mProvider.ui());
    }

    @Override
    public void populateNote() {
        mSubscriptions.add(mDb.getNote(mNoteId)
                .subscribeOn(mProvider.computation())
                .observeOn(mProvider.ui())
                .subscribe(
                        // onNext
                        note -> {
                            mView.setTitle(note.getTitle());
                            mView.setDescription(note.getDescription());

                        }, // onError
                        __ -> {
                            mView.showEmptyNoteError();

                        }));

    }

    @Override
    public Single<Boolean> deleteNote() {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDb.deleteNote(new Note(mNoteId));
                return true;
            }
        }).subscribeOn(mProvider.io()).observeOn(mProvider.ui());
    }


    private boolean isNewNote() {
        return mNoteId == null;
    }
}
